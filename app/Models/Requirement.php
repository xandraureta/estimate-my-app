<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Requirement extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function type()
    {
        return $this->belongsTo('App\Models\Type', 'type_id', 'id');
    }

    public function items()
    {
        return $this->hasMany('App\Models\Item', 'requirement_id', 'id');
    }
    
    public static function boot() 
    {
        parent::boot();

        static::deleting(function($requirement) 
        {
            foreach($requirement->items as $item) 
            {
                $item->delete();
            }
        });
    }
}
