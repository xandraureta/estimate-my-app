<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;
    use SoftDeletes;

    public function requirements()
    {
        return $this->hasMany('App\Models\Requirement', 'type_id', 'id');
    }
    
    public static function boot()
    {
        parent::boot();

        static::deleting(function ($type) 
        {
            foreach ($type->requirements as $requirement) 
            {
                foreach ($requirement->items as $item) 
                {
                    $item->delete();
                }
                $requirement->delete();
            }
        });
    }
}
