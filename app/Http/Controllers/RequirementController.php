<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Type;
use App\Models\Requirement;
use Illuminate\Http\Request;

class RequirementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requirements = Requirement::all();
        
        return view('admin.requirements.index', compact('requirements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'type_id' => 'required|integer',
            'description' => 'required|string',
        ]);

        $requirement = new Requirement();
        $requirement->type_id = $request->input('type_id');
        $requirement->description = $request->input('description');

        if ($requirement->save()) {
            return back()->with('success', 'Successfully added a new requirement.');
        }

        return back()->with('danger', 'Error in adding a new requirement.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Requirement $requirement
     * @return \Illuminate\Http\Response
     */
    public function show(Requirement $requirement)
    {
        return view('admin.requirements.show', compact('requirement'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $requirement = Requirement::find($id);

        $validatedData = $request->validate([
            'type_id' => 'required|integer',
            'description' => 'required|string',
        ]);

        $requirement->type_id = $request->input('type_id');
        $requirement->description = $request->input('description');

        if ($requirement->save()) {
            return back()->with('success', 'Successfully edited the requirement.');
        }

        return back()->with('danger', 'Error in editing the requirement.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $requirement = Requirement::find($id);
        if ($requirement->delete()) {
            Log::info(
                'A requirement has been deleted.',
                ['requirement_id' => $id],
                ['deleted_at' => Now()]
            );
            return back()->with('success', 'Successfully deleted the requirement');
        }
        return back()->with('danger', 'Error deleting the requirement.');
    }
}
