<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\Item;
use App\Models\Requirement;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function dashboard()
    {
        $types = Type::all();
        $items = Item::all();
        $requirements = Requirement::all();

        return view('admin.dashboard', compact('types', 'items', 'requirements'));
    }
}
