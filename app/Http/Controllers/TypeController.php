<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Type;
use App\Models\Item;
use Illuminate\Http\Request;

class TypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::all();
        
        return view('admin.types.index', compact('types'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
        ]);

        $type = new Type();
        $type->name = $request->input('name');
        $type->description = $request->input('description');

        if ($type->save()) {
            return back()->with('success', 'Successfully added a new type.');
        }

        return back()->with('danger', 'Error in adding a new type.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type  $type
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type)
    {
        return view('admin.types.show', compact('type'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $type = Type::find($id);

        $validatedData = $request->validate([
            'name' => 'required|string',
            'description' => 'required|string',
        ]);

        $type->name = $request->input('name');
        $type->description = $request->input('description');

        if ($type->save()) {
            return back()->with('success', 'Successfully edited the type.');
        }

        return back()->with('danger', 'Error in editing the type.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = Type::find($id);
        if ($type->delete()) {
            Log::info(
                'A type has been deleted.',
                ['type_id' => $id],
                ['deleted_at' => Now()]
            );
            return back()->with('success', 'Successfully deleted the type');
        }
        return back()->with('danger', 'Error deleting the type.');
    }
}
