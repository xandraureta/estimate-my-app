<?php

namespace App\Http\Controllers;

use Log;
use App\Models\Type;
use App\Models\Item;
use App\Models\Requirement;
use Illuminate\Http\Request;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'requirement_id' => 'required|integer',
            'description' => 'required|string',
            'type' => 'required|string',
            'value' => 'required|integer',
            'src' => 'required|string',
        ]);

        $item = new Item();
        $item->requirement_id = $request->input('requirement_id');
        $item->description = $request->input('description');
        $item->type = $request->input('type');
        $item->value = $request->input('value');
        $item->src = $request->input('src');

        if ($item->save()) {
            return back()->with('success', 'Successfully added a new item.');
        }

        return back()->with('danger', 'Error in adding a new item.');
    }

     /**
     * Display the specified resource.
     *
     * @param  \App\Models\Type $type
     * @param  \App\Models\Requirement $requirement
     * @param  \App\Models\Item $item
     * @return \Illuminate\Http\Response
     */
    public function show(Type $type, Requirement $requirement, Item $item)
    {
        return view('admin.items.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Item::find($id);

        $validatedData = $request->validate([
            'requirement_id' => 'required|integer',
            'description' => 'required|string',
            'type' => 'required|string',
            'value' => 'required|integer',
            'src' => 'required|string',
        ]);

        $item->requirement_id = $request->input('requirement_id');
        $item->description = $request->input('description');
        $item->type = $request->input('type');
        $item->value = $request->input('value');
        $item->src = $request->input('src');

        if ($item->save()) {
            return back()->with('success', 'Successfully edited the item.');
        }

        return back()->with('danger', 'Error in editing the item.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::find($id);
        if ($item->delete()) {
            Log::info(
                'An item has been deleted.',
                ['item_id' => $id],
                ['deleted_at' => Now()]
            );
            return back()->with('success', 'Successfully deleted the item');
        }
        return back()->with('danger', 'Error deleting the item.');
    }
}
