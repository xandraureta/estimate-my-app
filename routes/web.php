<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'PageController@index');

// Auth
Auth::routes(['register' => false]);
Route::prefix('admin')->name('admin.')->middleware(['auth'])->group(function () {
    // Dashboard
    Route::get('', 'AdminController@dashboard')->name('dashboard');

    // Types
    Route::resource('types', 'TypeController');

    // Requirements
    Route::resource('requirements', 'RequirementController');

    // Items
    Route::resource('items', 'ItemController');
});
