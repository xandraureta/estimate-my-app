$(document).ready(function () {

    // on scroll
    $(window).scroll(function () {
        if ($(window).scrollTop() > 300) {
            $('#scroll-to-top').addClass('show');
        }
        else {
            $('#scroll-to-top').removeClass('show');
        }
    });

    // on click
    $("#scroll-to-top").on('click', function (event) {
        $('html, body').animate({ scrollTop: 0 }, '500');
    });

});
