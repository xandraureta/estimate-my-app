$(document).ready(function () {
    
    $('#deleteConfirmationModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        $(this).find("form").attr("action", button.data("action"));
    });

    $('#editTypeModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var type = button.data('type');
        modal.find('#inputName').val(type.name);
        modal.find('#inputDescription').val(type.description);
        modal.find('#modalForm').attr('action', `/admin/types/${type.id}`);
    });

    $('#editRequirementModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var requirement = button.data('requirement');
        modal.find('#type_id').val(requirement.type_id);
        modal.find('#inputDescription').val(requirement.description);
        modal.find('#modalForm').attr('action', `/admin/requirements/${requirement.id}`);
    });

    $('#editItemModal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var modal = $(this);
        var item = button.data('item');
        modal.find('#inputItemLabel').val(item.type);
        modal.find('#inputItemDescription').val(item.description);
        modal.find('#inputItemValue').val(item.value);
        modal.find('#inputItemIcon').val(item.icon);
        modal.find('#modalForm').attr('action', `/admin/items/${item.id}`);
    });
});
