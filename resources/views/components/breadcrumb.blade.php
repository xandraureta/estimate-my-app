<div class="mt-4">
  <nav aria-label="breadcrumb">
    <ol class="breadcrumb flex-nowrap m-0 overflow-auto rounded-0">
      @if(isset($pages))
        @foreach($pages as $page)
          <li class="breadcrumb-item text-nowrap">
            <a href="{{ $page[1] }}">
              {{ $page[0] }}
            </a>
          </li>
        @endforeach
      @endif
      <li class="breadcrumb-item text-nowrap pr-3" aria-current="page">
        {{ $current_page }}
      </li>
    </ol>
  </nav>
</div>
