{{-- Bootstrap core CSS --}}
<link href="{{ mix('css/app.css') }}" rel="stylesheet">

{{-- Fonts --}}
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet" href="https://drive.google.com/uc?id=1jLrfISsLUcRiwgNl0koSfsS6lylj0E7h">

{{-- Custom Styles --}}
@yield('styles')