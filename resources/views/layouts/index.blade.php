<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    @include('layouts.meta')
    @include('layouts.css')
  </head>
  <body>
    <div class="mx-auto w-xl-75 bg-white p-0 ">

      {{-- @include('layouts.components.scroll-to-top') --}}

      @include('layouts.navbar')

      @yield('content')

    </div>
    <!--
    <div class="mx-auto w-xl-75 bg-white p-4 rounded">
      <h5 class="font-weight-bold">Save your estimate and get a permalink so you can share with your team</h5>
    
      <form class="new_estimate ng-pristine ng-valid" id="new_estimate" action="/" accept-charset="UTF-8" method="post"><input name="utf8" type="hidden" value="✓"><input type="hidden" name="authenticity_token" value="pg1+hvqSUMzb2Dzk6UWvvoMB5GBjveNcqbBmFYqc0GVZimSaTeQygFWum0zuLt//6YARl5fNIX/EGvixqg1J1w==">
        <div class="field">
          <input placeholder="Enter your email address" class="form-control" type="text" name="estimate[email]" id="estimate_email">
          <div class="mt-2">
            <input name="estimate[subscribe]" type="hidden" value="0"><input type="checkbox" value="1" checked="checked" name="estimate[subscribe]" id="estimate_subscribe">
            <label for="estimate_subscribe">
              Receive useful information about building apps in your inbox. No spam. Ever.
            </label>
          </div>

          <div class="actions">
            <input type="submit" name="commit" value="Save Estimate" class="btn btn-success btn-lg" data-disable-with="Save Estimate">
          </div>
        </form>
        
      </div>
    -->

    @include('layouts.js')

  </body>
</html>
