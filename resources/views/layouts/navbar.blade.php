<nav class="navbar navbar-white bg-white justify-content-between border-bottom border-secondary mb-4">
  <a class="navbar-brand text-dark">
    <img data-hidpi-src="https://d3h99m5mv5zvgz.cloudfront.net/assets/logo@2x-4bf33130a7ea759540af525dd4498a5c677159028773e399e450f084975ce2ff.png" src="https://d3h99m5mv5zvgz.cloudfront.net/assets/logo-4bf33130a7ea759540af525dd4498a5c677159028773e399e450f084975ce2ff.png" alt="Logo" height="31px">
  </a>
</nav>