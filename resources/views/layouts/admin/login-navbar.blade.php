<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top shadow">
  <div class="container">
    <a class="navbar-brand d-block mx-auto" href="/admin">{{ config('app.name', 'Estimate My App') }} Administrator Portal</a>
  </div>
</nav>
