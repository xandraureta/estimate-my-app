<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
  <head>
    {{-- Meta --}}
    @include('layouts.admin.meta')

    {{-- CSS --}}
    @include('layouts.admin.css')

  </head>
  <body id="admin" class="bg-light">

    <div id="app" class="page-container position-relative pt-4">

      <div class="content-container">
        @include('layouts.admin.top-navbar')
        
        <div class="container mt-4">
          @include('admin.components.alerts')
        </div>

        @yield('content')
      </div>

    </div>

    {{-- Javascript --}}
    @include('layouts.admin.js')

  </body>
</html>
