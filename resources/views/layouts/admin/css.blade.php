<link rel="stylesheet" href="{{ asset('css/admin.css') }}">

{{-- FontAwesome Icons --}}
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
