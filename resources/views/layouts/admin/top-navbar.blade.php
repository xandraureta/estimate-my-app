<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top shadow">

  <div class="d-flex ml-auto">

    <div class="mr-2">
    </div>

    <div class="dropdown ml-auto d-flex align-items-center">
      <a class="dropdown-toggle lead text-light text-decoration-none" href="#" id="dropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        {{ config('app.name', 'Estimate My App') }}
      </a>
      <div class="dropdown-menu bg-dark border-0 shadow dropdown-menu-right" aria-labelledby="navbarDropdown">
        <a class="dropdown-item text-light py-2" href="/" target="_blank">
          Go to Site
        </a>
        <a class="dropdown-item text-light py-2" href="{{ route('logout') }}" onclick="event.preventDefault();
        document.getElementById('logout-form').submit();">
          {{ __('Logout') }}
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
          @csrf
        </form>
      </div>
    </div>
  </div>

</nav>
