<link rel="stylesheet" href="{{ asset('css/auth.css') }}">

{{-- Google Fonts and Material Icon --}}
<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
