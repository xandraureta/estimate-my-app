<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" class="h-100">
  <head>
    {{-- Meta --}}
    @include('layouts.admin.meta')

    {{-- CSS --}}
    @include('layouts.auth.css')

  </head>
  <body class="bg-light">

    <div id="app" class="page-container position-relative d-flex">

      <div class="content-container align-self-center w-100 p-0">
        @include('layouts.admin.login-navbar')
        @yield('content')
      </div>

    </div>

    {{-- Javascript --}}
    <script src="{{ asset('js/auth.js') }}"></script>
    @yield('scripts')

  </body>
</html>
