@extends('layouts.index')

@section('styles')
  <style>
    body {
        background-color: #f5f5f5;
    }
     #Web-list .nav-link.active{
      background:#6cb2eb  ;
    }

    #iOS-list .nav-link.active{
      background:#ffed4a   ;
    }

    #Android-list .nav-link.active{
      background:#38c172   ;
    }

    .popover
    {
      background:#343a40;
    }

    .popover-body
    {
      color: white;
    }

    .bs-popover-bottom .arrow::after,
    .bs-popover-auto[x-placement^="bottom"] .arrow::after {
        border-bottom-color: #343a40;
    }

    .bs-popover-top .arrow::after,
    .bs-popover-auto[x-placement^="top"] .arrow::after {
        border-top-color: #343a40;
    }

    label > input[type="radio"]:checked + div > img, label > input[type="checkbox"]:checked + div > img {
      border: 10px solid #7BE891;
    }

    label > input[type="radio"]:checked + div::after, label > input[type="checkbox"]:checked + div::after {
      position: absolute;
      width: 50px;
      right: 10%;
      top: 20%;
      margin-top: -15px;
      font-family: 'Material Icons';
      font-size: 30px;
      color: #fff;
      content: "done";
      border-radius: 50%;
      background-color: #7BE891;
    }

    label > input[type="radio"], label > input[type="checkbox"] {
      visibility: hidden;
      position: absolute;
    }

    label > input[type="radio"] + div > img, label > input[type="checkbox"] + div > img{
      cursor: pointer;
      border: 10px solid transparent;
    }
    
  </style>
@endsection
        
@section('content')
    <div class="text-center p-lg-4 m-4">
        <p class="display-4 font-weight-bold">Estimate My App</p>
        <p class="h3 ">Select the items below which best describe your app and the features you require.</p>
        <p class="h5 text-muted my-3">All estimates are approximate but should give you a rough idea of what it will take to build your app.</p>
    </div>

    <ul class="nav nav-tabs text-center border border-0" id="myTab" role="tablist">
      @foreach ($allTypes as $type) 
        <li class="nav-item col-4 p-lg-3 p-2 
          @if(strcmp($type->name,'Web') == 0)
            bg-info
          @elseif(strcmp($type->name,'iOS') == 0)
            bg-warning 
          @elseif(strcmp($type->name,'Android') == 0)
            bg-success
          @endif"
          id="{{$type->name}}-list">
          <a class="border-0 text-white nav-link p-0
            @if($type->id == 1) 
              active
            @endif"
            id="{{$type->name}}-tab" data-toggle="tab" href="#{{$type->name}}" role="tab" aria-controls="{{$type->name}}" aria-selected="true">
            <p class="font-weight-bold h3">
              @if(strcmp($type->name,'Web') == 0)
                <i class="fa fa-desktop fa-fw mr-2">
              @elseif(strcmp($type->name,'iOS') == 0)
                <i class="fa fa-apple fa-fw mr-2">
              @elseif(strcmp($type->name,'Android') == 0)
                <i class="fa fa-android fa-fw mr-2">
              @endif
              </i>{{$type->name}}</p>
            <p class="px-lg-5 h5">{{$type->description}}</p>
          </a>
        </li>
      @endforeach
      </ul>

    <!-- Tab panes -->
    <div class="tab-content">
      @foreach ($allTypes as $type) 
      <div class="ml-0
        @if(strcmp($type->name,'Web') == 0)
          bg-info
        @elseif(strcmp($type->name,'iOS') == 0)
          bg-warning
        @elseif(strcmp($type->name,'Android') == 0)
          bg-success
        @endif
        tab-pane
        @if($type->id == 1)
          active
        @endif"
        id="{{$type->name}}" role="tabpanel" aria-labelledby="{{$type->name}}-tab">
        <div class="text-center">
          <div class="row pt-3 mr-0">
            @foreach ($type->requirements as $requirement)
              @if ($requirement->type_id == $type->id)
                <div class="px-4 mx-auto col-12 d-none d-xl-block">
                  <div class="bg-white rounded-sm m-4 p-5">
                    <div class="row">
                      <div class="col-12">
                      <h3 class="font-weight-bold my-4 mb-5">{{$loop->iteration}}. {{$requirement->description}}</h3>
                      </div>
                    </div>
                    <div class="row">
                      @foreach ($requirement->items as $item)
                        @if ($item->requirement_id == $requirement->id)
                          <div class="
                            @if($loop->parent->iteration <= 2)
                              col-4 
                            @else
                              col-3
                            @endif
                            p-3"
                            data-toggle="popover-hover" data-trigger="focus" data-placement="bottom" data-content="{{$item->description}}">
                            <label>
                              <h6>{{$item->type}}</h6>
                              @if($loop->parent->iteration == 1)
                                <input name="development-circle" type="radio" class="hidden {{$type->name}}-circle-development-checkbox">
                              @elseif($loop->parent->iteration == 2)
                                <input name="design-circle" type="radio" class="hidden {{$type->name}}-circle-design-checkbox">
                              @else
                                <input type="checkbox" class="hidden {{$type->name}}-circle-checkbox">
                              @endif
                              <div class="checkmark">
                                <img class="rounded-circle img-fluid" alt="{{$item->type}}" src="{{$item->src}}">
                              </div>
                            </label>
                          </div>
                        @endif
                      @endforeach
                    </div>
                  </div>
                </div>
                
                <div class="col-12 d-xl-none mr-0 pr-0">
                  <div class="bg-white rounded-sm m-4 p-lg-5 px-5 py-2">
                     <div class="row">
                      <div class="col-12">
                      <p class="font-weight-bold my-4 mb-lg-5 h3">{{$loop->iteration}}. {{$requirement->description}}</p>
                      </div>
                    </div>
                      @foreach ($requirement->items as $item)
                        @if ($item->requirement_id == $requirement->id)
                        <div class="row bg-light mb-3">
                          <div class="col-5 p-3">
                            <label>
                              @if($loop->parent->iteration == 1)
                                <input name="development-circle" type="radio" class="hidden {{$type->name}}-circle-development-checkbox">
                              @elseif($loop->parent->iteration == 2)
                                <input name="design-circle" type="radio" class="hidden {{$type->name}}-circle-design-checkbox">
                              @else
                                <input type="checkbox" class="hidden {{$type->name}}-circle-checkbox">
                              @endif
                              <div class="checkmark">
                                <img class="rounded-circle img-fluid" alt="{{$item->type}}" src="{{$item->src}}">
                              </div>
                            </label>
                          </div>
                          <div class="col-7 text-left align-self-center">
                            <div>
                              <p class="h4">{{$item->type}}</p>
                              <p class="text-muted h5">{{$item->description}}</p>
                            </div>
                          </div>
                        </div>
                        @endif
                      @endforeach
                  </div>
                </div>
              @endif
            @endforeach
            <div class="col-12 my-4 pb-4 pr-0">
              <a class="text-decoration-none text-white" data-toggle="collapse" href="#{{$type->name}}Calculation" role="button" aria-expanded="false" aria-controls="collapseExample">
                Click to Show Calculations
              </a>
              <div class="collapse mt-5" id="{{$type->name}}Calculation">
                <div class="col-12 px-lg-4 mx-auto">
                  <div class="bg-white text-dark rounded-sm {{$type->name}}-computation px-5 py-3 mx-2">
                    @foreach ($type->requirements as $requirement)
                      @if ($requirement->type_id == $type->id)
                        <div class="row pt-lg-5 pt-3">
                          <div class="col-md-11 col-9 text-left px-0">
                            <p class="h4 font-weight-bold">{{$loop->iteration}}. {{$requirement->description}}</p>
                          </div>
                          <div class="col-md-1 col-3 px-0">
                            <p class="h4 font-weight-bold">
                              @if($loop->iteration == 1)
                                Base Days
                              @elseif($loop->iteration == 2)
                                Percentage
                              @else
                                Days
                              @endif
                            </p>
                          </div>
                        </div>
                        @foreach ($requirement->items as $item)
                          @if ($item->requirement_id == $requirement->id)  
                            <div class="row border-top py-3">
                              <div class="col-md-11 col-9 px-0 text-left d-flex align-items-center">
                                @if($loop->parent->iteration == 1)
                                  <input name="development" type="radio" class="{{$type->name}}-computation-development" radioValue="{{$type->name}}-computation-checkbox-{{$item->id}}">
                                @elseif($loop->parent->iteration == 2)
                                  <input name="design" type="radio" class="{{$type->name}}-computation-design" radioValue="{{$type->name}}-computation-checkbox-{{$item->id}}">
                                @else
                                  <input type="checkbox" class="{{$type->name}}-computation-checkbox" checkboxValue="{{$type->name}}-computation-checkbox-{{$item->id}}">
                                @endif
                                <div class="px-1 h5 mb-0"> {{$item->type}} </div>
                              </div>
                              
                              <div class="col-md-1 col-3 px-0">
                                <input type="number" id="{{$type->name}}-computation-checkbox-{{$item->id}}" value="{{$item->value}}" min="0" max="50" class="form-control form-control-sm text-center px-n5 {{$type->name}}-computation-checkbox-value">
                              </div>
                            </div>
                          @endif
                        @endforeach
                      @endif
                    @endforeach

                    <div class="row font-weight-bold pt-lg-5 pt-3">
                      <div class="col-md-11 col-9 px-0  text-left">
                        <p class="h4 font-weight-bold ">{{count($type->requirements) + 1}}. App Specific Development</p>
                      </div>
                      <div class="col-md-1 col-3 px-0">
                        <p class="h4 font-weight-bold ">Days</p>
                      </div>
                    </div>

                    <div class="row border-top py-3">
                      <div class="col-md-11 col-9 px-0 text-left d-flex align-items-center">
                          <input type="checkbox" class="{{$type->name}}-specific-development-checkbox" checkboxValue="{{$type->name}}-specific-development-value">
                        <div class="px-1 h5 mb-0"> App Specific Development </div>
                      </div>
                      
                      <div class="col-md-1 col-3 px-0">
                        <input type="number" id="{{$type->name}}-specific-development-value" value="0" min="0" max="50" class="form-control form-control-sm text-center px-n5 {{$type->name}}-specific-computation">
                      </div>
                    </div>

                    <div class="row border-top py-3">
                      <div class="col-md-11 col-9 px-0 text-left d-flex align-items-center">
                          <input type="checkbox" class="{{$type->name}}-project-manager-checkbox" checkboxValue="{{$type->name}}-project-manager-value">
                        <div class="px-1 h5 mb-0"> Project Manager </div>
                      </div>
                      
                      <div class="col-md-1 col-3 px-0">
                        <input type="number" id="{{$type->name}}-project-manager-value" value="0" min="0" max="50" class="form-control form-control-sm text-center px-n5 {{$type->name}}-specific-computation">
                      </div>
                    </div>
                    
                    <div class="row pt-lg-5 pt-3">
                      <div class="col-md-10 col-7 px-0 text-left d-flex align-items-center">
                        <div class="h4 font-weight-bold"> Design Day Rate </div>
                      </div>
                      
                      <div class="col-md-2 col-5 px-0 input-group">
                        <div class="input-group">
                          <div class="input-group-text input-group-sm text-white bg-secondary">$</div>
                          <input type="number" id="{{$type->name}}-design-day-rate" value="450" min="0" class="form-control text-center px-n5">
                        </div>
                        </div>
                    </div>

                    <div class="row pt-2">
                      <div class="col-md-10 col-7 px-0 text-left d-flex align-items-center">
                        <div class="h4 font-weight-bold"> Developer Day Rate </div>
                      </div>
                      
                      <div class="col-md-2 col-5 px-0 input-group">
                        <div class="input-group">
                          <div class="input-group-text input-group-sm text-white bg-secondary">$</div>
                          <input type="number" id="{{$type->name}}-developer-day-rate" value="450" min="0" class="form-control text-center px-n5">
                        </div>
                        </div>
                    </div>

                    <div id="{{$type->name}}-project-manager-rate" class="row pt-2 d-none">
                      <div class="col-md-10 col-7 px-0 text-left d-flex align-items-center">
                        <div class="h4 font-weight-bold"> Project Manager Day Rate </div>
                      </div>
                      
                      <div class="col-md-2 col-5 px-0 input-group">
                        <div class="input-group">
                          <div class="input-group-text input-group-sm text-white bg-secondary">$</div>
                          <input type="number" id="{{$type->name}}-project-manager-day-rate" value="100" min="0" class="form-control text-center px-n5">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      @endforeach
      <ul id="footer-nav" class="nav nav-tabs text-center" role="tablist">
        @foreach ($allTypes as $type) 
          <li class="nav-item col-4 p-lg-3 p-2 d-flex flex-column
            @if(strcmp($type->name,'Web') == 0)
              bg-info
            @elseif(strcmp($type->name,'iOS') == 0)
              bg-warning 
            @elseif(strcmp($type->name,'Android') == 0)
              bg-success
            @endif"
            id="{{$type->name}}-list">
            <a class="border-0 text-white nav-link text-left p-0 d-none
              @if($type->id == 1) 
                active
              @endif"
                id="{{$type->name}}-cost-tab" data-toggle="tab" href="#{{$type->name}}" role="tab" aria-controls="{{$type->name}}" aria-selected="true">
                <p class="border-bottom border-white h4 py-lg-3">
                  @if(strcmp($type->name,'Web') == 0)
                    <i class="fa fa-desktop fa-fw mr-2">
                  @elseif(strcmp($type->name,'iOS') == 0)
                    <i class="fa fa-apple fa-fw mr-2">
                  @elseif(strcmp($type->name,'Android') == 0)
                    <i class="fa fa-android fa-fw mr-2">
                  @endif
                    </i>{{$type->name}} App</p>
                <p class="my-lg-4 h6" id="{{$type->name}}-total-designer"></p>
                <p class="my-lg-4 h6" id="{{$type->name}}-total-developer"></p>
                <p class="my-lg-4  mb-0 h2" id="{{$type->name}}-total-cost"></p>
            </a>
            <a class="border-0 text-white nav-link my-auto p-0
              @if($type->id == 1) 
                active
              @endif"
                id="{{$type->name}}-get-service" data-toggle="tab" href="#{{$type->name}}" role="tab" aria-controls="{{$type->name}}" aria-selected="true">
                <p class="h2 font-weight-bold">
                @if(strcmp($type->name,'Web') == 0)
                  <i class="fa fa-desktop d-none d-xl-block mb-3"></i>
                  Need a {{$type->name}} App?</p>
                  <p class="h5">(or a back-end to your mobile app)</p>
                @elseif(strcmp($type->name,'iOS') == 0)
                  <i class="fa fa-apple d-none d-xl-block mb-3"></i>
                  Need a {{$type->name}} App?</p>
                @elseif(strcmp($type->name,'Android') == 0)
                  <i class="fa fa-android d-none d-xl-block mb-3"></i>
                  Need a {{$type->name}} App?</p>
                @endif
            </a>
          </li>
        @endforeach
      </ul>
          
    </div>
    
    <div class="p-4 my-4 rounded">
      <p class="display-4 font-weight-bold" id="Total-cost">Total Cost: $0</p>
      <p class="h3">Based on Oozou rates. To use your own rates, click <em>Show Calculations</em> above.</p>
      <p class="h5 text-muted"><small>Please note, all cost estimates are intended to be indicative of development costs and timescales only and are exclusive of all hosting costs, paid services or purchased assets of any kind. All prices are in USD and inclusive of sales tax.</small></p>
    </div>
@endsection

@section('scripts')
    <script>
      $(function () {
        $("#footer-nav.nav-tabs li.nav-item a.nav-link").click(function() {
          $("#footer-nav.nav-tabs li.nav-item a.nav-link").removeClass('active');
        });

        $('[data-toggle="popover-hover"]').popover({trigger: 'hover'});

        var totalCost = 0;
        var webCost = 0;
        var iOSCost = 0;
        var androidCost = 0;

        $(".Web-computation-development").change(function() { webComputeCost(); });
        
        $(".Web-circle-development-checkbox").change(function() { 
          $( ".Web-circle-development-checkbox" ).each(function( index ) {
            $('input.Web-computation-development').eq(index).prop('checked',this.checked);
          });

          webComputeCost(); 
        });

        $(".Web-computation-development").change(function() { 
          $( ".Web-computation-development" ).each(function( index ) {
            $('input.Web-circle-development-checkbox').eq(index).prop('checked',this.checked);
          });

          webComputeCost(); 
        });
        
        $(".Web-circle-design-checkbox").change(function() { 
          $( ".Web-circle-design-checkbox" ).each(function( index ) {
            $('input.Web-computation-design').eq(index).prop('checked',this.checked);
          });

          webComputeCost(); 
        });

        $(".Web-computation-design").change(function() { 
          $( ".Web-computation-design" ).each(function( index ) {
            $('input.Web-circle-design-checkbox').eq(index).prop('checked',this.checked);
          });

          webComputeCost(); 
        });

        $(".Web-circle-checkbox").change(function() { 
          
          $( ".Web-circle-checkbox" ).each(function( index ) {
            $('input.Web-computation-checkbox').eq(index).prop('checked',this.checked);
          });

          webComputeCost(); 
        });
        
        $(".Web-computation-checkbox").change(function() { 
          
          $( ".Web-computation-checkbox" ).each(function( index ) {
            $('input.Web-circle-checkbox').eq(index).prop('checked',this.checked);
          });

          webComputeCost(); 
        });
        
        $(".Web-computation-checkbox-value").change(function() { webComputeCost(); });
        
        $("#Web-design-day-rate").change(function() { webComputeCost(); });
        
        $("#Web-developer-day-rate").change(function() { webComputeCost(); });

        $("#Web-project-manager-value").change(function() { webComputeCost(); });

        $(".Web-project-manager-checkbox").change(function() {
            $('.Web-project-manager-checkbox').prop('checked',this.checked);

            if (this.checked) {
              $("#Web-project-manager-rate").removeClass('d-none');
            }
            else
            {
              $("#Web-project-manager-rate").addClass('d-none');
            }

          webComputeCost(); 
        });

        $(".Web-specific-development-checkbox").change(function() { webComputeCost(); });

        $(".Web-computation-design-checkbox").change(function() { webComputeCost(); });

        $("#Web-specific-development-value").change(function() { 
          $('#Web-project-manager-value').val($(this).val());
          webComputeCost(); 
        });

        if ($(".Web-project-manager-checkbox").is(":checked")) {  
          $("#Web-project-manager-rate").removeClass('d-none');
        }
        else
        {
          $("#Web-project-manager-rate").addClass('d-none');
        }

        var webComputeCost = function() {
          var totalDays = 0;
          var designerWeek = 0;
          var developerWeek = 0;
          var designerDays = 0;
          var totalCost = 0;
          var projectManager = 0;

          var developerItem = $("input:radio[name=development].Web-computation-development:checked")[0];
          var desginerItem = $("input:radio[name=design].Web-computation-design:checked")[0];

          var developer = developerItem == null ? 0 : $("#" + developerItem.getAttribute('radioValue')).val();
          var designer = desginerItem == null ? 0 : $("#" + desginerItem.getAttribute('radioValue')).val();

          $( ".Web-computation-checkbox" ).each(function( index ) {
            if (this.checked) {
              totalDays += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          $( ".Web-specific-development-checkbox" ).each(function( index ) {
            if (this.checked) {
              totalDays += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          $( ".Web-project-manager-checkbox" ).each(function( index ) {
            if (this.checked) {
              projectManager += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          baseDays = Math.round((developer *  designer) / 100);
          designerWeek = baseDays % 5 == 0 ? (0.2 * baseDays) : (0.2 * baseDays).toFixed(1);

          developerWeek =  parseFloat(developerWeek) + parseFloat(designerDays);

          totalDays = parseFloat(totalDays) + parseFloat(developer);
          developerWeek = totalDays % 5 == 0 ? (0.2 * totalDays) : (0.2 * totalDays).toFixed(1);
          totalCost = (baseDays * $("#Web-design-day-rate").val()) + totalDays * $("#Web-developer-day-rate").val() + projectManager * $("#Web-project-manager-day-rate").val();

          if (totalCost > 0)
          {
            $("#Web-cost-tab").removeClass("d-none");
            $("#Web-get-service").addClass("d-none");
          }
          else
          {
            $("#Web-cost-tab").addClass("d-none");
            $("#Web-get-service").removeClass("d-none");
          }

          webCost = totalCost;
          totalCost = totalCost.toLocaleString().replace(/^0+/, '');
          
          $("#Web-total-designer").html(baseDays + " Designer Days ("+ designerWeek +" Weeks)");
          $("#Web-total-developer").html(totalDays + " Developer Days ("+ developerWeek +" Weeks)");
          $("#Web-total-cost").html("$" + totalCost);

          totalComputeCost();
        }

        $(".iOS-computation-development").change(function() { iOSComputeCost(); });

        $(".iOS-computation-design").change(function() { iOSComputeCost(); });

        $(".iOS-circle-development-checkbox").change(function() { 
          $( ".iOS-circle-development-checkbox" ).each(function( index ) {
            $('input.iOS-computation-development').eq(index).prop('checked',this.checked);
          });

          iOSComputeCost(); 
        });

        $(".iOS-computation-development").change(function() { 
          $( ".iOS-computation-development" ).each(function( index ) {
            $('input.iOS-circle-development-checkbox').eq(index).prop('checked',this.checked);
          });

          iOSComputeCost(); 
        });
        
        $(".iOS-circle-design-checkbox").change(function() { 
          $( ".iOS-circle-design-checkbox" ).each(function( index ) {
            $('input.iOS-computation-design').eq(index).prop('checked',this.checked);
          });

          iOSComputeCost(); 
        });

        $(".iOS-computation-design").change(function() { 
          $( ".iOS-computation-design" ).each(function( index ) {
            $('input.iOS-circle-design-checkbox').eq(index).prop('checked',this.checked);
          });

          iOSComputeCost(); 
        });

        $(".iOS-circle-checkbox").change(function() { 
          
          $( ".iOS-circle-checkbox" ).each(function( index ) {
            $('input.iOS-computation-checkbox').eq(index).prop('checked',this.checked);
          });

          iOSComputeCost(); 
        });
        
        $(".iOS-computation-checkbox").change(function() { 
          
          $( ".iOS-computation-checkbox" ).each(function( index ) {
            $('input.iOS-circle-checkbox').eq(index).prop('checked',this.checked);
          });

          iOSComputeCost(); 
        });
        
        $(".iOS-computation-checkbox-value").change(function() { iOSComputeCost(); });
        
        $("#iOS-design-day-rate").change(function() { iOSComputeCost(); });
        
        $("#iOS-developer-day-rate").change(function() { iOSComputeCost(); });

        $("#iOS-project-manager-value").change(function() { iOSComputeCost(); });

        $(".iOS-project-manager-checkbox").change(function() {
            $('.iOS-project-manager-checkbox').prop('checked',this.checked);

            if (this.checked) {
              $("#iOS-project-manager-rate").removeClass('d-none');
            }
            else
            {
              $("#iOS-project-manager-rate").addClass('d-none');
            }

            iOSComputeCost(); 
        });

        $(".iOS-specific-development-checkbox").change(function() { iOSComputeCost(); });

        $(".iOS-computation-design-checkbox").change(function() { iOSComputeCost(); });

        $("#iOS-specific-development-value").change(function() { 
          $('#iOS-project-manager-value').val($(this).val());
          iOSComputeCost(); 
        });

        if ($(".iOS-project-manager-checkbox").is(":checked")) {  
          $("#iOS-project-manager-rate").removeClass('d-none');
        }
        else
        {
          $("#iOS-project-manager-rate").addClass('d-none');
        }

        var iOSComputeCost = function() {
          var totalDays = 0;
          var designerWeek = 0;
          var developerWeek = 0;
          var designerDays = 0;
          var totalCost = 0;
          var projectManager = 0;

          var developerItem = $("input:radio[name=development].iOS-computation-development:checked")[0];
          var desginerItem = $("input:radio[name=design].iOS-computation-design:checked")[0];

          var developer = developerItem == null ? 0 : $("#" + developerItem.getAttribute('radioValue')).val();
          var designer = desginerItem == null ? 0 : $("#" + desginerItem.getAttribute('radioValue')).val();

          $( ".iOS-computation-checkbox" ).each(function( index ) {
            if (this.checked) {
              totalDays += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          $( ".iOS-specific-development-checkbox" ).each(function( index ) {
            if (this.checked) {
              totalDays += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          $( ".iOS-project-manager-checkbox" ).each(function( index ) {
            if (this.checked) {
              projectManager += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          baseDays = Math.round((developer *  designer) / 100);
          designerWeek = baseDays % 5 == 0 ? (0.2 * baseDays) : (0.2 * baseDays).toFixed(1);

          developerWeek =  parseFloat(developerWeek) + parseFloat(designerDays);

          totalDays = parseFloat(totalDays) + parseFloat(developer);
          developerWeek = totalDays % 5 == 0 ? (0.2 * totalDays) : (0.2 * totalDays).toFixed(1);

          totalCost = (baseDays * $("#iOS-design-day-rate").val()) + totalDays * $("#iOS-developer-day-rate").val() + projectManager * $("#iOS-project-manager-day-rate").val();
          
          if (totalCost > 0)
          {
            $("#iOS-cost-tab").removeClass("d-none");
            $("#iOS-get-service").addClass("d-none");
          }
          else
          {
            $("#iOS-cost-tab").addClass("d-none");
            $("#iOS-get-service").removeClass("d-none");
          }
          
          iOSCost = totalCost;
          totalCost = totalCost.toLocaleString().replace(/^0+/, '');
          
          $("#iOS-total-designer").html(baseDays + " Designer Days ("+ designerWeek +" Weeks)");
          $("#iOS-total-developer").html(totalDays + " Developer Days ("+ developerWeek +" Weeks)");
          $("#iOS-total-cost").html("$" + totalCost);
          
          totalComputeCost();
        }

        $(".Android-computation-development").change(function() { androidComputeCost(); });

        $(".Android-circle-development-checkbox").change(function() { 
          $( ".Android-circle-development-checkbox" ).each(function( index ) {
            $('input.Android-computation-development').eq(index).prop('checked',this.checked);
          });

          androidComputeCost(); 
        });

        $(".Android-computation-development").change(function() { 
          $( ".Android-computation-development" ).each(function( index ) {
            $('input.Android-circle-development-checkbox').eq(index).prop('checked',this.checked);
          });

          androidComputeCost(); 
        });
        
        $(".Android-circle-design-checkbox").change(function() { 
          $( ".Android-circle-design-checkbox" ).each(function( index ) {
            $('input.Android-computation-design').eq(index).prop('checked',this.checked);
          });

          androidComputeCost(); 
        });

        $(".Android-computation-design").change(function() { 
          $( ".Android-computation-design" ).each(function( index ) {
            $('input.Android-circle-design-checkbox').eq(index).prop('checked',this.checked);
          });

          androidComputeCost(); 
        });

        $(".Android-circle-checkbox").change(function() { 
          
          $( ".Android-circle-checkbox" ).each(function( index ) {
            $('input.Android-computation-checkbox').eq(index).prop('checked',this.checked);
          });

          androidComputeCost(); 
        });
        
        $(".Android-computation-checkbox").change(function() { 
          
          $( ".Android-computation-checkbox" ).each(function( index ) {
            $('input.Android-circle-checkbox').eq(index).prop('checked',this.checked);
          });

          androidComputeCost(); 
        });
        
        $(".Android-computation-checkbox-value").change(function() { androidComputeCost(); });
        
        $("#Android-design-day-rate").change(function() { androidComputeCost(); });
        
        $("#Android-developer-day-rate").change(function() { androidComputeCost(); });

        $("#Android-project-manager-value").change(function() { androidComputeCost(); });

        $(".Android-project-manager-checkbox").change(function() {
            $('.Android-project-manager-checkbox').prop('checked',this.checked);

            if (this.checked) {
              $("#Android-project-manager-rate").removeClass('d-none');
            }
            else
            {
              $("#Android-project-manager-rate").addClass('d-none');
            }

            androidComputeCost(); 
        });

        $(".Android-specific-development-checkbox").change(function() { androidComputeCost(); });

        $(".Android-computation-design-checkbox").change(function() { androidComputeCost(); });

        $("#Android-specific-development-value").change(function() { 
          $('#Android-project-manager-value').val($(this).val());
          androidComputeCost(); 
        });

        if ($(".Android-project-manager-checkbox").is(":checked")) {  
          $("#Android-project-manager-rate").removeClass('d-none');
        }
        else
        {
          $("#Android-project-manager-rate").addClass('d-none');
        }

        var androidComputeCost = function() {
          var totalDays = 0;
          var designerWeek = 0;
          var developerWeek = 0;
          var designerDays = 0;
          var totalCost = 0;
          var projectManager = 0;

          var developerItem = $("input:radio[name=development].Android-computation-development:checked")[0];
          var desginerItem = $("input:radio[name=design].Android-computation-design:checked")[0];

          var developer = developerItem == null ? 0 : $("#" + developerItem.getAttribute('radioValue')).val();
          var designer = desginerItem == null ? 0 : $("#" + desginerItem.getAttribute('radioValue')).val();

          $( ".Android-computation-checkbox" ).each(function( index ) {
            if (this.checked) {
              totalDays += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          $( ".Android-specific-development-checkbox" ).each(function( index ) {
            if (this.checked) {
              totalDays += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });

          $( ".Android-project-manager-checkbox" ).each(function( index ) {
            if (this.checked) {
              projectManager += parseFloat($("#" + this.getAttribute('checkboxValue')).val());
            }
          });


          baseDays = Math.round((developer *  designer) / 100);
          designerWeek = baseDays % 5 == 0 ? (0.2 * baseDays) : (0.2 * baseDays).toFixed(1);

          developerWeek =  parseFloat(developerWeek) + parseFloat(designerDays);

          totalDays = parseFloat(totalDays) + parseFloat(developer);
          developerWeek = totalDays % 5 == 0 ? (0.2 * totalDays) : (0.2 * totalDays).toFixed(1);

          totalCost = (baseDays * $("#Android-design-day-rate").val()) + totalDays * $("#Android-developer-day-rate").val() + projectManager * $("#iOS-project-manager-day-rate").val();

          if (totalCost > 0)
          {
            $("#Android-cost-tab").removeClass("d-none");
            $("#Android-get-service").addClass("d-none");
          }
          else
          {
            $("#Android-cost-tab").addClass("d-none");
            $("#Android-get-service").removeClass("d-none");
          }

          androidCost = totalCost;
          totalCost = totalCost.toLocaleString().replace(/^0+/, '');
          
          $("#Android-total-designer").html(baseDays + " Designer Days ("+ designerWeek +" Weeks)");
          $("#Android-total-developer").html(totalDays + " Developer Days ("+ developerWeek +" Weeks)");
          $("#Android-total-cost").html("$" + totalCost);

          totalComputeCost();
        }

        var totalComputeCost = function() {
          var totalCost = 0;
          totalCost = androidCost + webCost + iOSCost;

          if (totalCost > 0)
          {
            totalCost = totalCost.toLocaleString().replace(/^0+/, '');
          }

          $("#Total-cost").html("Total Cost: $" + totalCost);
        }

        webComputeCost();
        iOSComputeCost();
        androidComputeCost();

      }) 
    </script>
@endsection
