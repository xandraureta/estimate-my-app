<div class="modal fade" id="editRequirementModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="modalForm" method="POST">
        @method('PUT')
        @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="editRequirementTitle">Edit requirement</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

            {{-- Requirement Id --}}
            <input type="hidden" id="type_id" name="type_id" value="">

            <div class="form-group">
              <label for="inputDescription" class="bmd-label-floating">Description</label>
              <input type="text" class="form-control" id="inputDescription" name="description" required>
              <small class="form-text text-muted">Question that you want to ask</small>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Edit</button>
        </div>
      </form>
    </div>
  </div>
</div>
