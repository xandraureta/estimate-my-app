<div class="modal fade" id="editTypeModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="modalForm" method="POST">
        @method('PUT')
        @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="editTypeTitle">Edit type</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          
          {{-- Name --}}
          <div class="form-group">
            <label for="inputName" class="bmd-label-floating">Name</label>
            <input type="text" class="form-control" id="inputName" name="name" required>
          </div>

          <div class="form-group">
            <label for="inputDescription" class="bmd-label-floating">Description</label>
            <input type="text" class="form-control" id="inputDescription" name="description" required>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Edit</button>
        </div>
      </form>
    </div>
  </div>
</div>
