<div class="modal fade" id="editItemModal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <form id="modalForm" method="POST">
        @method('PUT')
        @csrf
        <div class="modal-header">
          <h5 class="modal-title" id="editItemTitle">Edit item</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">

          {{-- Type Id --}}
          <input type="hidden" id="requirement_id" name="requirement_id" value="">

          {{-- Item Label --}}
          <div class="form-group">
            <label for="inputItemLabel" class="bmd-label-floating">Label</label>
            <input type="text" class="form-control" id="inputItemLabel" name="type" required>
          </div>

          {{-- Description Name --}}
          <div class="form-group">
            <label for="inputItemDescription" class="bmd-label-floating">Description</label>
            <textarea type="text" class="form-control" id="inputItemDescription" name="description"></textarea>
          </div>

          {{-- Price --}}
          <div class="form-group">
            <label for="inputItemValue" class="bmd-label-floating">Value</label>
            <input type="number" min="0" class="form-control" id="inputItemValue" name="value" min="0">
          </div>

          {{-- Icon Name --}}
          <div class="form-group">
            <label for="inputItemIcon" class="bmd-label-floating">Icon</label>
            <textarea type="text" class="form-control" id="inputItemIcon" name="src"></textarea>
          </div>


        </div>
        <div class="modal-footer">
          <button type="button" class="btn" data-dismiss="modal">Cancel</button>
          <button type="submit" class="btn btn-outline-success">Edit</button>
        </div>
      </form>
    </div>
  </div>
</div>
