<card-basic title="Requirements" class="mt-4">
  <template v-slot:options>
    <options-dropdown>
      <button type="button" class="dropdown-item" data-toggle="modal" data-target="#addrequirementModal">
        Add Requirement
      </button>
    </options-dropdown>
  </template>
  <template v-slot:image>
    <div class="table-responsive table-hover">
      <table class="table table-hover text-truncate text-center m-0">
        <thead>
          <tr>
            <th scope="col">Description</th>
            <th scope="col">Choices</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody class="text-wrap align-items-center">
          @foreach($type->requirements as $requirement)
            <tr>
              <td> {{ $requirement->description }} </td>
              <td>
                <a href="{{ route('admin.requirements.show',['requirement' => $requirement->id]) }}" > Show choices </a>
              </td>
              <td>
                <options-dropdown icon="more_horiz">
                  <button 
                    class="dropdown-item" type="button"
                    data-toggle="modal"
                    data-target="#editRequirementModal"
                    data-requirement="{{$requirement}}">
                    <i class="material-icons material-icons-outlined text-dark mr-2">edit</i>
                    Edit
                  </button>
                  <button 
                    class="dropdown-item text-danger" type="button"
                    data-toggle="modal"
                    data-target="#deleteConfirmationModal"
                    data-action="{{ route('admin.requirements.destroy', ['requirement' => $requirement->id]) }}">
                    <i class="material-icons material-icons-outlined text-danger mr-2 w-25">delete_outlined</i>
                    Delete
                </button>
                </options-dropdown>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </template>
  <template v-slot:footer>
    <button type="button" class="btn btn-primary btn-raised float-right" data-toggle="modal" data-target="#addRequirementModal">
      Add Requirement
    </button>
  </template>
</card-basic>
