<card-basic title="Types" class="mt-4">
  <template v-slot:image>
    <div class="table-responsive">
      <table class="table table-hover text-truncate text-center m-0">
        <thead>
          <tr>
            <th scope="col">Name</th>
            <th scope="col">Description</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody class="text-wrap align-items-center">
          @foreach($types as $type)
            <tr>
              <td>
                <a href="{{ route('admin.types.show', ['type' => $type->id]) }}"> {{ $type->name }} </a>
              </td>
              <td>
                {{ $type->description }}
              </td>
              <td>
                <options-dropdown icon="more_horiz">
                  <button 
                    class="dropdown-item" type="button"
                    data-toggle="modal"
                    data-target="#editTypeModal"
                    data-type="{{$type}}">
                    <i class="material-icons material-icons-outlined text-dark mr-2">edit</i>
                    Edit
                  </button>
                  <button 
                    class="dropdown-item text-danger" type="button"
                    data-toggle="modal"
                    data-target="#deleteConfirmationModal"
                    data-action="{{ route('admin.types.destroy', ['type' => $type->id]) }}">
                    <i class="material-icons material-icons-outlined text-danger mr-2 w-25">delete_outlined</i>
                    Delete
                </button>
                </options-dropdown>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </template>
  <template v-slot:footer>
    <button type="button" class="btn btn-primary btn-raised float-right" data-toggle="modal" data-target="#addTypeModal">
      Add Type
    </button>
  </template>
</card-basic>
