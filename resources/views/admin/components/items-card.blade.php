<card-basic title="Choices" class="mt-4">
  <template v-slot:options>
    <options-dropdown>
      <button type="button" class="dropdown-item" data-toggle="modal" data-target="#addrequirementModal">
        Add Requirement
      </button>
    </options-dropdown>
  </template>
  <template v-slot:image>
    <div class="table-responsive table-hover">
      <table class="table table-hover text-truncate text-center m-0">
        <thead>
          <tr>
            <th scope="col">Item Label</th>
            <th scope="col">Description</th>
            <th scope="col">Value</th>
            <th scope="col">Icon</th>
            <th scope="col"></th>
          </tr>
        </thead>
        <tbody class="text-wrap align-items-center">
          @foreach($requirement->items as $item)
            <tr>
              <td class="align-middle">
                {{$item->type}}
              </td>
              <td class="align-middle w-50">
                {{$item->description}}
              </td>
              <td class="align-middle">
                {{$item->value}}
              </td>
              <td  class="w-25">
                <img class="rounded-circle img-fluid w-25" src="{{$item->src}}">
              </td>
              <td class="align-middle">
                <options-dropdown icon="more_horiz">
                  <button
                    class="dropdown-item" type="button"
                    data-toggle="modal"
                    data-target="#editItemModal"
                    data-item="{{$item}}">
                    <i class="material-icons material-icons-outlined text-dark mr-2">edit</i>
                    Edit
                  </button>
                  <button 
                    class="dropdown-item text-danger" type="button"
                    data-toggle="modal"
                    data-target="#deleteConfirmationModal"
                    data-action="{{ route('admin.items.destroy', ['item' => $item->id]) }}">
                    <i class="material-icons material-icons-outlined text-danger mr-2 w-25">delete_outlined</i>
                    Delete
                </button>
                </options-dropdown>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>
  </template>
  <template v-slot:footer>
    <button type="button" class="btn btn-primary btn-raised float-right" data-toggle="modal" data-target="#addItemModal">
      Add Item
    </button>
  </template>
</card-basic>
