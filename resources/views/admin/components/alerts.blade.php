{{-- status alerts --}}
@if(session('status'))
  <div class="alert alert-admin alert-success alert-dismissible fade show">
    <div class="d-flex align-content-center">
      <div class="w-100 py-2">
        <p class="mb-0">
          {{ session('status') }}
        </p>
      </div>
      <button type="button" class="btn" data-dismiss="alert" aria-label="Close">
        <span class="material-icons" aria-hidden="true">close</span>
      </button>
    </div>
  </div>
@endif
{{-- primary alerts --}}
@if(session('primary'))
  <div class="alert alert-admin alert-primary alert-dismissible fade show">
    <div class="d-flex align-content-center">
      <div class="w-100 py-2">
        <p class="mb-0">
          {{ session('primary') }}
        </p>
      </div>
      <button type="button" class="btn" data-dismiss="alert" aria-label="Close">
        <span class="material-icons" aria-hidden="true">close</span>
      </button>
    </div>
  </div>
@endif
{{-- success alerts --}}
@if(session('success'))
  <div class="alert alert-admin alert-success alert-dismissible fade show">
    <div class="d-flex align-content-center">
      <div class="w-100 py-2">
        <p class="mb-0">
          {{ session('success') }}
        </p>
      </div>
      <button type="button" class="btn" data-dismiss="alert" aria-label="Close">
        <span class="material-icons" aria-hidden="true">close</span>
      </button>
    </div>
  </div>
@endif
{{-- warning alerts --}}
@if(session('warning'))
  <div class="alert alert-admin alert-warning alert-dismissible fade show">
    <div class="d-flex align-content-center">
      <div class="w-100 py-2">
        <p class="mb-0">
          {{ session('warning') }}
        </p>
      </div>
      <button type="button" class="btn" data-dismiss="alert" aria-label="Close">
        <span class="material-icons" aria-hidden="true">close</span>
      </button>
    </div>
  </div>
@endif
{{-- danger alerts --}}
@if(session('danger'))
  <div class="alert alert-admin alert-danger alert-dismissible fade show">
    <div class="d-flex align-content-center">
      <div class="w-100 py-2">
        <p class="mb-0">
          {{ session('danger') }}
        </p>
      </div>
      <button type="button" class="btn" data-dismiss="alert" aria-label="Close">
        <span class="material-icons" aria-hidden="true">close</span>
      </button>
    </div>
  </div>
@endif
{{-- error alerts --}}
@if($errors->any())
  <div class="alert alert-admin alert-danger alert-dismissible fade show">
    <div class="container d-flex align-content-center">
      <div class="w-100 py-2">
        <p class="mb-0">
          @foreach ($errors->all() as $error)
            {{ $error }}
            <br>
          @endforeach
        </p>
      </div>
      <button type="button" class="btn" data-dismiss="alert" aria-label="Close">
        <span class="material-icons" aria-hidden="true">close</span>
      </button>
    </div>
  </div>
@endif
