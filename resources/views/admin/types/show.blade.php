@extends('layouts.admin.index')

@section('content')
  <div class="container">
    
    @include('admin.components.modals.requirement-create')
    @include('admin.components.modals.requirement-edit')
    @include('admin.components.modals.delete-confirmation')

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Types', route('admin.types.index')),
      )))
      @slot('current_page', $type->name)
    @endcomponent

    @include('admin.components.requirements-card')

  </div>
@endsection
