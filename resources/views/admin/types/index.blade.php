@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @include('admin.components.modals.type-create')
    @include('admin.components.modals.type-edit')
    @include('admin.components.modals.delete-confirmation')

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
      )))
      @slot('current_page', 'Types')
    @endcomponent

    @include('admin.components.types-card')

  </div>
@endsection
