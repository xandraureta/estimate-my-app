@extends('layouts.admin.index')

@section('styles')
@endsection

@section('content')
  <div class="container">

    @component('components.breadcrumb')
      @slot('current_page', 'Dashboard')
    @endcomponent

    <card-basic title="Hello Admin!" class="mt-4">
      <a href="/" target="_blank">
        Visit Website
        &nbsp;
        <i class="material-icons md-18">exit_to_app</i>
      </a>
    </card-basic>

    @include('admin.components.modals.type-create')

    @include('admin.components.types-card')

  </div>
@endsection

@section('scripts')
  <script>
    $(document).ready(function() {
      
    });
  </script>
@endsection
