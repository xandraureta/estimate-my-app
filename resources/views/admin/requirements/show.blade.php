@extends('layouts.admin.index')

@section('content')
  <div class="container">

    @include('admin.components.modals.item-create')
    @include('admin.components.modals.item-edit')
    @include('admin.components.modals.delete-confirmation')

    @component('components.breadcrumb')
      @slot('pages', array(
        array('Dashboard', route('admin.dashboard')),
        array('Types', route('admin.types.index')),
        array($requirement->type->name,  route('admin.types.show', ['type' => $requirement->type->id])),
      )))
      @slot('current_page', $requirement->description)
    @endcomponent

    @include('admin.components.items-card')

  </div>
@endsection
